//
//  UserInfoGetable.swift
//  SWAG
//
//  Created by Mark33699 on 2019/8/15.
//  Copyright © 2019 eLove_ePhone. All rights reserved.
//

import UIKit

protocol UserInfoGetable
{
    var bioTextView: UITextView! { get }
    var avatarImageView: UIImageView! { get }
    
    func getUserInfo()
}

extension UserInfoGetable
{
    func getUserInfo()
    {
        //這裡假設不可重複登入, 而每次登入後都會取得最新的bio跟avatar存在userDefault裡, 要用的話就不用跟API要
        if let bio = UserDefaults.standard.value(forKey: kBio) as? String
        {
            bioTextView.text = bio
        }
        if let avatarName = UserDefaults.standard.value(forKey: kAvatar) as? String //這邊應該是取URL或Data, 因為沒有Server, 就先取本地圖片
        {
            avatarImageView.image = UIImage.init(named: avatarName)
        }
    }
}
