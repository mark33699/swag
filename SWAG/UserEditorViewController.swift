//
//  UserEditorViewController.swift
//  SWAG
//
//  Created by Mark33699 on 2019/8/14.
//  Copyright © 2019 eLove_ePhone. All rights reserved.
//

import UIKit

class UserEditorViewController: UIViewController, UserInfoGetable, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @IBOutlet weak var saveButtonItem: UIBarButtonItem!
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var textCountLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var constraintTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTextViewBottom: NSLayoutConstraint!
    
    //可以work的API
//    let bioURLString = "https://httpbin.org/patch"
//    let avatarURLString = "https://catbox.moe/user/api.php"
    let bioURLString = "https://api.mock.com/me"
    let avatarURLString = "https://api.mock.com/me/avatar"
    let textViewBottom: CGFloat = 25
    let maxTextCount = 200
    
    var originBio = ""
    var didPickedImage = false
    var apiCount = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        getUserInfo()
        textCountLabel.text = "\(bioTextView.text.count)/\(maxTextCount)"
        originBio = bioTextView.text
        
        let popButtonItem = UIBarButtonItem.init(title: "⬅️", style: .plain, target: self, action: #selector(popVC))
        self.navigationItem.leftBarButtonItem = popButtonItem //把leftItem換掉, 以免被手勢返回
        
        NotificationCenter.default.addObserver(self, selector: #selector(willShowKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func updateBio()
    {
        let request = NSMutableURLRequest(url: NSURL(string: bioURLString)! as URL)
        request.httpMethod = "PATCH"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do{
            let json = ["bio": bioTextView.text]
            let jsonData = try JSONSerialization.data(withJSONObject: json)
            request.httpBody = jsonData
            print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
        }
        catch 
        {
            print("ERROR")
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            DispatchQueue.main.async
            {
                UserDefaults.standard.set(self.bioTextView.text, forKey: kBio)
                self.apiCompletion()
            }
        }
        task.resume()
    }
    
    func uploadAvatar()
    {
        guard let image = avatarImageView.image else { return  }
        
        let boundary = UUID().uuidString
        let filename = "avatar.png"
        let fieldName = "reqtype"
        let fieldValue = "fileupload"
        let fieldName2 = "userhash"
        let fieldValue2 = "3cecb70223e15f7fe6273b926"
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        var urlRequest = URLRequest(url: URL(string: avatarURLString)!)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue)".data(using: .utf8)!)
        
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName2)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue2)".data(using: .utf8)!)
        
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"fileToUpload\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(image.pngData()!)
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        session.uploadTask(with: urlRequest, from: data, completionHandler:
        { responseData, response, error in
            UserDefaults.standard.set("avatar", forKey: kAvatar) //雖然是fake API, 但應該是要更新成圖片URL, 所以就不特別把選好的圖片存下來了
            self.apiCompletion()
        }).resume()
    }
    
    func apiCompletion()//假設都是正常回來200, 就不去管error了
    {
        apiCount -= 1
        if apiCount == 0
        {
            DispatchQueue.main.async
            {
                self.loadingView.isHidden = true
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    //MARK: IBAction
    
    @IBAction func save(_ sender: Any)
    {
        bioTextView.resignFirstResponder()//以免鍵盤蓋在loadingView上
        
        let appDLG = UIApplication.shared.delegate as! AppDelegate
        appDLG.window?.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        
        if didPickedImage
        {
            apiCount += 1
            uploadAvatar()
        }
        
        if bioTextView.text != originBio
        {
            apiCount += 1
            updateBio()
        }
    }
    
    @IBAction func changeAvatar(_ sender: Any)
    {
        let imagePicker = UIImagePickerController.init()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: Delegate Method
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if textView.markedTextRange != nil
        {
            return true
        }
        
        var textCount = 0
        if text.count == 0
        {
            textCount = textView.text.count - range.length
        }
        else
        {
            textCount = textView.text.count + text.count
        }
        
        return !(textCount > maxTextCount)
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView.markedTextRange != nil
        {
            return
        }
        
        if textView.text.count > maxTextCount //如果用quickType一次選多個字, shouldChangeTextInRange攔不到
        {
            textView.text = "\(textView.text.prefix(maxTextCount))"
        }
        
        textCountLabel.text = "\(textView.text.count)/\(maxTextCount)"
        if didPickedImage == false
        {
            saveButtonItem.isEnabled = bioTextView.text != originBio
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let image = info[.originalImage] as? UIImage
        {
            avatarImageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
        saveButtonItem.isEnabled = true
        didPickedImage = true
    }
    
    //MARK: Selector
    
    @objc func popVC()
    {
        bioTextView.resignFirstResponder()//以免選擇完Alert後鍵盤又彈起來
        
        if saveButtonItem.isEnabled
        {
            let alert = UIAlertController.init(title: "資料尚未儲存", message: "請問是否確認放棄？", preferredStyle: .alert)
            let actionYes = UIAlertAction.init(title: "是", style: .destructive)
            { (action) in
                self.navigationController?.popViewController(animated: true)
            }
            let actionNo = UIAlertAction.init(title: "否", style: .default, handler: nil)
            alert.addAction(actionYes)
            alert.addAction(actionNo)
            present(alert, animated: true, completion: nil)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func willShowKeyboard(notif: NSNotification)
    {
        if let userInfo = notif.userInfo, let rect = userInfo["UIKeyboardBoundsUserInfoKey"] as? CGRect
        {
            constraintTextViewBottom.constant = textViewBottom + rect.size.height
        }
    }
    
    @objc func willHideKeyboard(notif: NSNotification)
    {
        constraintTextViewBottom.constant = textViewBottom
    }
}
