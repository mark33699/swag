//
//  UserViewController.swift
//  SWAG
//
//  Created by Mark33699 on 2019/8/14.
//  Copyright © 2019 eLove_ePhone. All rights reserved.
//

import UIKit

class UserViewController: UIViewController, UserInfoGetable
{
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.isHidden = true
        getUserInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.isHidden = false
    }
}

